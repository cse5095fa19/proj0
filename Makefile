.PHONY : pdf
pdf : proj0.pdf
%.pdf : %.tex
	latexmk -pdf $<

.PHONY : clean
clean :
	latexmk -C
