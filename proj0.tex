\documentclass{article}

\usepackage{fancyhdr}
\usepackage[margin=0.55in,top=1in,bottom=1.25in,headheight=0pt]{geometry}
\usepackage{titling}
\usepackage[inline]{enumitem}
\usepackage{cleveref}
\usepackage{graphicx}
\graphicspath{{figures/}}
\usepackage{wrapfig}
\usepackage{floatrow}
\floatsetup{heightadjust=object} % Align floatrow figures to top
\usepackage{tikz}
\usetikzlibrary{arrows.meta,shapes.arrows}
\tikzset{>=latex}

\title{Model and measure polymerase wavefronts and intragenic activity\vspace{-10pt}}
\setlength{\droptitle}{-50pt}
\posttitle{\par\end{center}}    % chktex 10
\preauthor{}
\postauthor{}
\predate{}
\postdate{}
\date{}

\pagestyle{fancy}
\lhead{Project 0}
\chead{CSE 5095 Bayesian Machine Learning}
\rhead{Pariksheet Nanda}
\lfoot{\today}
% Version scheme:
% x._ Major version is number of times an advisor or colleague has reviewed.
% _.x Minor version denotes significant changes since the review.
%
\rfoot{Version 2.0}
% 2.0 - Reviewed by Prof. Derek Aguiar.  Applied corrections.
% 1.1 - Cleaned up text; relate data to the new model.
% 1.0 - First review by Prof. Derek Aguiar; added initial graphical model.
% 0.1 - No reviews yet.  Will present at Prof. Derek Aguiar's office.

\begin{document}
\maketitle
\thispagestyle{fancy}
% What scientific problems or hypotheses do you find interesting or care about?

% Build on or use ideas that we cover in class.

% Model development: what research questions can you formulate on this data?
% What are the variables?  Which are observed or unobserved?  How do they relate
% to eachother?  How do you perform inference and model checking?

% Review: perform an in-depth literature survey of a class of models or
% inference algorithms related to what we discussed in class.
\noindent
Robust measurements of gene activity can be obtained
with statistical modeling.
In contrast to statistical modeling,
traditional genomic arithmetic which
is simpler
for exploratory data analysis and inference,
but genomic arithmetic is less capable at controlling for
missing data and extraneous data.
Therefore,
the additional effort to
develop models
is suitable for measurements that are
unreliable using
traditional genomic arithmetic alone.
The  Precision Run-On and Sequencing (PROseq) assay
provides unbiased, genome-wide, instantaneous positions
of polymerase molecules,
as shown in \cref{fig:proseq}.
I will measure several characteristics of gene transcription activity
using PROseq data by developing Bayesian probabilistic graphical models.

\begin{figure}[H]
  \begin{floatrow}
    \ffigbox[\FBwidth][\FBheight][t]{
      \caption{
        \textbf{The PROseq protocol.}
        PROseq locates the instantaneous position of polymerase molecules
        that are actively transcribing through genes,
        unlike RNAseq which indirectly infers gene activity from
        accumulation of mature transcripts.
        The instantaneous position property of PROseq
        allows direct modeling of gene activity.
      }\label{fig:proseq}}{
      \begin{tikzpicture}
        \node at (-1.2, 0) {\includegraphics[width=3.6in]{proseq}};
        % Legend.
        \draw (-5.7, -3.5) rectangle (-2, 0.2);
        \node[right] at (-4.7,-0.2) {Polymerase};
        \node[right] at (-4.7,-0.8) {Nascent RNA};
        \node[right] at (-4.7,-1.4) {Br-UTP};
        \node[right] at (-4.7,-2.0) {Nuclear RNA};
        \node[right] at (-4.7,-2.6) {5' adapter};
        \node[right] at (-4.7,-3.2) {3' adapter};
        % Annotations.
        \node[above] at (-5.0, 2.7) {unengaged};
        \node[below] at (-3.5, 1.1) {engaged};
        \node[above right] at (0.9,2.4) {Sarkosyl};
        % Connections.
        \node[above,text width=1.8cm] at (-1.6,2.2) {Run-on + NT analog};
        \node[shape=single arrow,draw,minimum height=1cm] at (-1.2,1.8) {};
        \node[right,text width=1.8cm] at (1.3,0) {Purify + ligate adapters};
        \node[shape=single arrow,draw,minimum height=1cm,shape border rotate=270] at (1.0,0) {};
      \end{tikzpicture}
    }
    \ffigbox[\FBwidth][\FBheight][t]{
      \caption{
        \textbf{CCNG2 gene PROseq time series.}
        The CCNG2 gene show a typical wave of polymerase during mitosis
        running hundreds of kilobases past the annotated
        termination site of the gene.
        The transcription termination machinery becomes active
        sometime between the 3 hour and 6 hour timepoints,
        allowing the gene boundary to return to a location
        comparable to asynchronous cells.
        Red and blue tracks indicate transcription
        on the positive and negative strands respectively.
      }\label{fig:raw}}{
      \begin{tikzpicture}
        % Browser session: https://genome.ucsc.edu/s/pariksheet/ccng2_proposal
        \node at (-0.7, 0) {\includegraphics[width=0.38\textwidth]{raw}};
        % Annotations.
        \node[left] at (-4.7, 2.51) {CCNG2};
        \node[left] at (-4.7, 1.80) {0 hrs};
        \node[left] at (-4.7, 1.37) {1 hr\phantom{s}};
        \node[left] at (-4.7, 0.94) {1.5 hrs};
        \node[left] at (-4.7, 0.50) {2 hrs};
        \node[left] at (-4.7, 0.07) {3 hrs};
        \node[left] at (-4.7,-0.36) {4 hrs};
        \node[left] at (-4.7,-0.79) {6 hrs};
        \node[left] at (-4.7,-1.22) {8 hrs};
        \node[left] at (-4.7,-1.66) {12 hrs};
        \node[left] at (-4.7,-2.09) {Async};
      \end{tikzpicture}
    }
  \end{floatrow}
\end{figure}

\noindent
The data for my project is unpublished time series PROseq reads
collected by Luke Wojenski and Leighton Core
of HeLa cells undergoing mitosis.
A novel discovery from observing nascent RNA from cells
going through mitosis, is transcription continues uninterrupted far
beyond the normal gene boundary,
but a few hours into mitosis the normal gene boundary is restored
once the cellular machinery for transcription termination is restored.
A typical gene, CCNG2, shown in \cref{fig:raw}
illustrates how the 1.5--4 hour timepoints
continue hundreds of kilobases beyond the typical gene boundary
of asynchronous cells.
The typical boundary is restored somewhere between the 3 and 6 hour timepoints.
We're interested in measuring
each gene's transcription rate,
activation time, and elongation boundary.

My modeling goal is to
measure the boundary where the wave of polymerase transcription ends
at each timepoint;
all other measurements, of transcription rate and activation time,
are dependent on accurate boundary positions.
\Cref{fig:counts} shows a heatmap with a na\"{\i}ve estimate of the
boundary positions shown with blue pixels.
Determining these boundary positions is made difficult by
\begin{enumerate*}[label= (\roman*)]
\item the sparseness of data.
The 4 hour timepoint in \cref{fig:raw}, for example,
has fewer reads than the 3 hour timepoint
because transcription termination removes transcripts
and the reads appear to ``decay'' as more transcripts are removed
between the 4 --8 hour timepoints.
The preliminary model in \cref{fig:model} models this decay with the
global parameter, $\lambda$.
Additional methods for dealing with the sparseness of data
are detailed in the next paragraph.
\item A more difficult challenge with determining accurate boundaries
is subtracting out additional signal from enhancers shown in \cref{fig:counts}.
The top 20\% rows of \cref{fig:counts}
show how the  na\"{\i}vely calculated boundary, shown with blue pixels,
overestimates the gene transcription boundary due to additional
signal from intragenic enhancers.
I will use a mixture random variable for removing the contribution
of these reads from the gene of interest,
indicated by $Z_{nt}$ categorical and $\psi_t$ mixture variables
in \cref{fig:model},
and each gene has a corresponding
activation time $A_{gt}$,
and termination time $\tau_{gt}$.
\end{enumerate*}

\begin{figure}
  \fcapside[2.5in]{
    \caption{
      \textbf{Graphical model of mixtures of gene extension and intagenic
        activity.}
      Our $Z$ and $\psi$ variables mix
      the contributions of  all intragenic activity with the primary gene.
      Instead of considering an infinite number of genes,
      consider only the de novo genes (G) found by the \texttt{dREG} software
      for now.
      The spatial data is indexed by the total number of non-overlapping windows
      along the length of our gene of interest (N),
      and the temporal data is indexed by our discrete timepoints (T).
      We assume that all the positional windows are independent,
      and only assume time dependence.
      We will later attempt to extend this model with a Dirichlet process.
    }\label{fig:model}}{
    \begin{tikzpicture}
      [
      scale=2.0,
      var/.style={draw,circle,minimum size=1cm},
      observed/.style={draw,circle,minimum size=1cm,fill=gray!20}
      ]
      % \draw [help lines] (0, 0) grid (4.5, 3.5);
      % Variables
      \node[var] (l) at (1, 3) {$\lambda$};
      \node[observed] at (2, 3) (m) {$M_n$};
      \node[observed] at (2, 2) (x) {$X_{nt}$};
      \node[var] (z) at (3, 2) {$Z_{nt}$};
      \node[var] (p) at (1, 2) {$\psi_t$};
      \node[var] (t) at (3, 1) {$\tau_{gt}$};
      \node[var] (a) at (2, 1) {$A_{gt}$};
      % Arrows
      \draw [<-] (x) -- (m);
      \draw [<-] (x) -- (z);
      \draw [<-] (x) -- (l);
      \draw [<-] (x) -- (p);
      \draw [<-] (z) -- (t);
      \draw [<-] (z) -- (a);
      % Plates
      \draw (1.6, 1.6) rectangle (3.4, 3.4);
      \node[above left] at (3.4, 1.6) {N};
      \draw (1.6, 0.6) rectangle (3.4, 1.4);
      \node[above left] at (3.4, 0.6) {G};
      \draw (0.6, 0.4) rectangle (3.6, 2.4);
      \node[above left] at (3.6, 0.4) {T};
    \end{tikzpicture}
  }
\end{figure}

% TODO Create subfigure label insets using llap and raisebox:
% https://tex.stackexchange.com/questions/89776/image-inside-another-image#89778
\begin{figure}[H]
  \fcapside[4.5in]{
    \caption{
      \textbf{Missing data from unmappable regions
        and unwanted data from intragenic enhancers.}
      The heatmap shows genes longer than 360 kilobases ($n = 179$)
      at the 1.5 hour timepoint
      arranged in rows and sorted by
      the position of the the blue polymerase wave end window.
      Reads are aggregated into 50 basepair windows.
      Red windows contain reads,
      and windows containing fewer reads are linearly more transparent.
      White windows contain unmappble regions which cannot have read counts.
      Green windows are putative promoters and intragenic enhancers
      as determined by the \texttt{dREG} software.
      Blue windows are the transcribed-untranscribed bounaries
      where the polymerase wave ends,
      na\"{\i}vely determined using read distributions alone.
      The boxplots show the per gene prevalence of
      missing (unmappable) and unwanted (\texttt{dREG}) artifacts.
      The top 20\% of the heatmap shows how additional unwanted reads from the
      intragenic enhancer artifacts causes the
      the na\"{\i}ve algorithm to overshoot the wave end.
    }\label{fig:counts}}{
    \input{hmmvsmcmc.tex} \\
    \input{artifacts.tex}
  }
\end{figure}

The published approach for determining the transcribed-untranscribed boundary
is fitting an exponential distribution to the histogram of reads.
Strictly speaking,
it is statistically incorrect
to fit a continuous distribution like the exponential
to discrete data of read counts.
However, in practise, the exponential distribution has
much stronger discrimination power than
an equivalent discrete distribution
like the Poisson distribution.
The likely reason is the number of zero-values is important to distinguish
untranscribed from transcribed regions,
but the Poisson ignores the number of zero-values.
I will improve on existing practice
using a zero-inflated Poisson distribution,
or a zero-inflated negative-binomial distribution, to
find the transcription boundary.

All nascent RNA sequencing technologies suffer
from missing reads due to
repetitive unmappable regions in the genome.
Preliminary modeling demonstrates it is trivial to control for missing data
due to these unmappable regions of the genome;
in a Bayesian framework
one simply sets $\log(1) = 0$ in the posterior spatial windows
of these unmappable regions.
The observed unmappable regions are modeled
by the variable $M_n$ in \cref{fig:model}.

\end{document}
